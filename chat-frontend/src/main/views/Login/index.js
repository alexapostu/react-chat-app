import React from "react";
import {Button, Card, Heading, Pane, TextInputField} from "evergreen-ui";

class Login extends React.Component {

    render = () => (
        <Pane className={'page-container'}>
            <Card className={'back-white pad-small width-512 m-top-128'} elevation={2}>
                <Heading size={800}>Login</Heading>
                <br/>
                <TextInputField label={'Username'}/>
                <TextInputField type={'password'} label={'Password'}/>
                <Button float={'right'} appearance="primary">Login</Button>
            </Card>
        </Pane>
    )
}

export default Login;