package com.alexapostu.chatbackend.service;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.alexapostu.chatbackend.dao.UsersDAO;
import com.alexapostu.chatbackend.service.entity.User;
import com.alexapostu.chatbackend.security.AuthenticationProvider;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	private final UsersDAO usersDAO;
	private final BCryptPasswordEncoder encoder;
	private final UUID uuid;
	private final AuthenticationProvider authProvider;

	private AuthenticationServiceImpl(final UsersDAO usersDAO, final BCryptPasswordEncoder encoder, final UUID uuid,
			final AuthenticationProvider authProvider) {
		this.usersDAO = usersDAO;
		this.encoder = encoder;
		this.uuid = uuid;
		this.authProvider = authProvider;
	}

	@Override
	public Optional<String> login(final String username, final String password) {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);
		Optional<User> user = usersDAO.findByUsername(username);
		if (user.isPresent() && encoder.matches(password, user.get().getPassword())) {
			String token = uuid.randomUUID().toString();
			authProvider.authenticateUser(token, user.get());
			return Optional.of(token);
		}
		throw new BadCredentialsException("Unable to authenticate using provided credentials.");
	}
}
