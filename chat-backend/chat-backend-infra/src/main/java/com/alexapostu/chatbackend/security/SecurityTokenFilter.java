package com.alexapostu.chatbackend.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Component
public class SecurityTokenFilter extends HttpFilter {

    @Override
    protected void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain) throws IOException, ServletException {
        if (SecurityContextHolder.getContext().getAuthentication() == null && request.getCookies() != null) {
            Optional<Cookie> authCookie = Arrays.stream(request.getCookies()).filter(cookie -> AUTHORIZATION.equals(cookie.getName())).findFirst();
            if (authCookie.isPresent()) {
                String authToken = authCookie.get().getValue();
                Authentication requestAuthentication = new UsernamePasswordAuthenticationToken(authToken, authToken);
                SecurityContextHolder.getContext().setAuthentication(requestAuthentication);
            }
        }

        chain.doFilter(request, response);
    }
}
