package com.alexapostu.chatbackend.security;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.alexapostu.chatbackend.service.entity.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	private static final List<GrantedAuthority> USER_ROLES = Collections.singletonList(new SimpleGrantedAuthority("user"));
	private final Map<String, UserDetails> authenticatedUsers = new ConcurrentHashMap<>();


	@Override
	protected void additionalAuthenticationChecks(final UserDetails userDetails,
			final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
		// No additional checks
	}

	@Override
	protected UserDetails retrieveUser(final String userName, final UsernamePasswordAuthenticationToken authenticationToken) throws AuthenticationException {
		String accessToken = (String) authenticationToken.getCredentials();
		if (accessToken == null || !authenticatedUsers.containsKey(accessToken)) {
			throw new PreAuthenticatedCredentialsNotFoundException("Unable to determine user based on access token.");
		}
		return authenticatedUsers.get(accessToken);
	}

	public void authenticateUser(final String accessToken, final User user) {
		authenticatedUsers.put(accessToken, new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				USER_ROLES));
	}
}

