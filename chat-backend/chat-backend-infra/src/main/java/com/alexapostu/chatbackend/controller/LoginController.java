package com.alexapostu.chatbackend.controller;

import com.alexapostu.chatbackend.service.AuthenticationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
public class LoginController {

    private final AuthenticationService authenticationService;

    public LoginController(final AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }


    @GetMapping("/login")
    public void login(HttpServletResponse response, @RequestParam final String username, @RequestParam final String password) {
        Optional<String> token = authenticationService.login(username, password);
        token.ifPresent(s -> response.addCookie(new Cookie(AUTHORIZATION, s)));
    }
}
