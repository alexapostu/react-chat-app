package com.alexapostu.chatbackend.service;

import java.util.Optional;

/**
 * An authentication service which can return an authentication token based on user credentials.
 */
public interface AuthenticationService {

	/**
	 * Login the user and return an authentication token if possible.
	 * @param username The username.
	 * @param password The password.
	 * @return A user authentication token if authentication was successful, empty otherwise.
	 */
	Optional<String> login(final String username, final String password);
}
