package com.alexapostu.chatbackend.service.dao;

import com.alexapostu.chatbackend.service.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;

@Repository
public interface UsersDAO extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);
}
